'use strict'

const path = require('path')
const { Eta } = require('eta')
const cluster = require('cluster')
const numCPUs = require('os').cpus().length
const server = require('fastify')({
  logger: true,
  maxParamLength: 100
})

/**
 * Set Environment
 */
const SetEnvironment = async () => {
  await server.register(require('@fastify/env'), {
    dotenv: {
      path: '.env'
    },
    schema: {
      type: 'object',
      required: ['USEWORKER', 'ENVIRONMENT', 'HOST'],
      properties: {
        USEWORKER: { type: 'boolean', default: 'false' },
        ENVIRONMENT: { type: 'string', default: 'development' },
        HOST: { type: 'string', default: '0.0.0.0' },
        PORT: { type: 'number' }
      }
    }
  })
}

/**
 * Register Plugins and Routes
 */
const Register = async () => {
  server.register(require('@fastify/cookie'))
  server.register(require('@fastify/session'), {
    cookieName: 'sessionId',
    secret: 'a secret with minimum length of 32 characters',
    cookie: { secure: false },
    expires: (3600000 * 8) // 8h
  })

  server.register(require('@fastify/view'), {
    engine: {
      eta: new Eta()
    },
    root: path.join(__dirname, 'views'),
    viewExt: 'html',
    options: {
      production: false
    }
  })

  server.register(require('@fastify/static'), {
    root: path.join(__dirname, 'public'),
    prefix: '/',
    maxAge: (3600 * 1000),
    immutable: true,
    decorateReply: false
  })

  // Load all custom plugins (hooks, routes, etc) should be on the last
  server.register(require('@fastify/autoload'), {
    dir: path.join(__dirname, 'plugins'),
    dirNameRoutePrefix: false,
    maxDepth: 2
  })
}

/**
 * Run Server
 */
const Run = async () => {
  try {
    await server.listen({ host: server.config.HOST, port: (server.config.PORT || process.env.PORT) })
    console.log('Server is listening on ' + process.env.HOST + ':' + (server.config.PORT || process.env.PORT))
  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}

/**
 * Bootstrap Application
 */
const Bootstrap = async () => {
  await SetEnvironment()
  if (server.config.USEWORKER) {
    if (cluster.isMaster) {
      console.log(`Master ${process.pid} is running`)
      for (let i = 0; i < numCPUs; i++) {
        cluster.fork()
      }
      cluster.on('exit', worker => {
        console.log(`Worker ${worker.process.pid} died`)
      })
    } else {
      await Register()
      await Run()
    }
  } else {
    await Register()
    await Run()
  }
}
// Start Application
Bootstrap()
