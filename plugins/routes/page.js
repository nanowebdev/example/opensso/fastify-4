async function pageRoute (server, options) {
  server.get('/', async (request, reply) => {
    if (request.query.token) {
      // set session
      request.session.token = request.query.token
      // redirect to dashboard
      return reply.redirect('/dashboard')
    }
    const html = await server.view('home', { title: 'Homepage' })
    await reply
      .header('Content-Type', 'text/html; charset=utf-8')
      .send(html)
  })

  server.get('/dashboard', { preHandler: [server.checkSession, server.verifyToken] }, async (request, reply) => {
    const jwt = await server.decodeJwt(request.session.token)
    const html = await server.view('dashboard', { title: 'Dashboard', jwt: jwt.payload })
    await reply
      .header('Content-Type', 'text/html; charset=utf-8')
      .send(html)
  })

  server.get('/logout', async (request, reply) => {
    // clear session if any
    if (request.session.token) {
      request.session.destroy()
    }
    return await reply.redirect('/')
  })
}

module.exports = pageRoute
