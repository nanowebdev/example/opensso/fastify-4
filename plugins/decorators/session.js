/**
 * checkSession
 */
'use strict'

const fp = require('fastify-plugin')

async function session (app) {
  app.decorate('checkSession', async function (request, reply) {
    if (!request.session.token) {
      return await reply.redirect('/')
    }
  })
}

module.exports = fp(session)
