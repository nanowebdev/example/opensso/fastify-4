/**
 * JWT
 */
'use strict'

const fs = require('fs')
const jwt = require('jsonwebtoken')
const fp = require('fastify-plugin')

const publicKEY = fs.readFileSync('public.key', 'utf8')
const privateKEY = fs.readFileSync('private.key', 'utf8')

const jwtOptions = {
  expiresIn: '8h',
  algorithm: 'RS256'
}

function _verify (token, callback) {
  jwt.verify(token, publicKEY, jwtOptions, function (err, decode) {
    if (err) return callback(err)
    return callback(null, decode)
  })
}

function _decode (token) {
  return jwt.decode(token, { complete: true })
}

function _sign (payload, callback) {
  jwt.sign(payload, privateKEY, jwtOptions, function (err, token) {
    if (err) return callback(err)
    return callback(null, token)
  })
}

async function JWT (app, opts) {
  app.decorate('verifyToken', async function (request, reply) {
    _verify(request.session.token, function (err, decode) {
      if (err) return reply.redirect('/')
    })
  })

  app.decorate('decodeJwt', async (token) => {
    return _decode(token)
  })

  app.decorate('signJwt', async (payload) => {
    return new Promise((resolve, reject) => {
      _sign(payload, function (err, data) {
        if (err) return reject(err)
        resolve(data)
      })
    })
  })

  app.decorate('verifyJwt', async (token) => {
    return new Promise((resolve, reject) => {
      _verify(token, function (err, data) {
        if (err) return reject(err)
        resolve(data)
      })
    })
  })
}

module.exports = fp(JWT)
