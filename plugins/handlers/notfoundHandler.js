async function handlerNotFound (server, options) {
  server.setNotFoundHandler(async function (request, reply) {
    if (request.raw.url.indexOf('/api/') !== -1) {
      reply.code(404).send({
        statusCode: 404,
        message: 'Route ' + request.method + ':' + request.url + ' not found!',
        error: 'Not Found'
      })
    } else {
      let _detail = ''
      for (const key in request.headers) {
        _detail += key + ': ' + request.headers[key] + '\n'
      }
      const html = await server.view('error', {
        code: 404,
        title: 'Not Found',
        message: 'Sorry, the page you are looking for has been deleted or moved.',
        path: request.url,
        detail: _detail,
        time: new Date().toISOString()
      })
      reply
        .code(404)
        .header('Content-Type', 'text/html; charset=utf-8')
        .send(html)
    }
    await reply
  })
}

module.exports = handlerNotFound
