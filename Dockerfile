FROM node:16-alpine

WORKDIR /app/fastify-4

COPY package*.json ./

RUN npm install

COPY . ./

EXPOSE 8000